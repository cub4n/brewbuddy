package com.cub4n.abvcalculator;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Menu;

public class Changelog extends Activity {

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_changelog);

		int deviceVersion = android.os.Build.VERSION.SDK_INT;
		Log.d("version", Integer.toString(deviceVersion));
		if (deviceVersion > 10) {
			ActionBar bar = getActionBar();
			ColorDrawable myColor = new ColorDrawable(
					Color.parseColor("#825A36"));
			bar.setBackgroundDrawable(myColor);
			// bar.setBackgroundDrawable(new
			// ColorDrawable(Color.parseColor("#603814")));
		}

	}

}
