package com.cub4n.abvcalculator;

public class Event implements Comparable{
	
	private int time;
	private String eventTitle;
	
	public Event(String eventTitle, int time) {
		this.time = time;
		this.eventTitle = eventTitle;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public String getEventTitle() {
		return eventTitle;
	}

	public void setEventTitle(String eventTitle) {
		this.eventTitle = eventTitle;
	}

	@Override
	public int compareTo(Object another) {
		// TODO Auto-generated method stub
		if(((Event) another).getTime() == time)
			return 0;
		else if (((Event) another).getTime() < time)
			return -1;
		else 
			return 1;
	}


}
