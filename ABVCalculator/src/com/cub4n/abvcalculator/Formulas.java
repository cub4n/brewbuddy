package com.cub4n.abvcalculator;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class Formulas {

	private int version = 19;
	private int currentVersion;
	private String myVersion = "1.4.1.0";
	private String updates = "~" + myVersion + "~\n\n\t* Added Mash Calculator with data caching.\n\t* Changed first time start to default to settings for initialization.\n\t* Fixed calorie calculation for Metric users.";

	public Formulas() {

	}

	public int getVersion() {
		return version;
	}

	public int getCurrentVersion() {
		return currentVersion;
	}

	public String getMyVersion() {
		return myVersion;
	}

	public String getUpdates() {
		return updates;
	}

	// convert Fahrenheit to Celcius
	public double FtoC(double f) {

		return (f - 32) * (5.0 / 9.0);

	}

	// convert Celcius to Fahrenheit
	public double CtoF(double c) {

		return c * (9.0 / 5.0) + 32.0;

	}

	// calculates abv
	public double calculateABV(double OG, double FG) {
		return ((1.05 / .79) * ((OG - FG) / FG) * 100);
	}

	// calculates abw
	public double calculateABW(double percentAlcohol, double FG) {

		return (.79 * percentAlcohol) / FG;
	}

	// converts original brix to original gravity
	public double OBtoOG(double OB, double wortCorrection) {

		return ((OB / wortCorrection) / (258.6 - ((OB / 258.2) * 227.1))) + 1;

	}

	// converts final brix to final gravity
	public double FBtoFG(double OB, double FB) {

		return 1.0111958 - 0.00813003 * OB + 0.0144032 * FB + 0.000523555 * OB * OB - 0.00166862 * FB * FB - 0.0000125754 * OB * OB * OB + 0.0000812663 * FB * FB * FB;

	}

	// formula used to calculate ABV with tempurature correction
	public double calculation(double gravity, double temp, double calibration) {

		return gravity * ((1.00130346 - 0.000134722124 * temp + 0.00000204052596 * Math.pow(temp, 2) - 0.00000000232820948 * Math.pow(temp, 3)) / (1.00130346 - 0.000134722124 * calibration + 0.00000204052596 * Math.pow(calibration, 2) - 0.00000000232820948 * Math.pow(calibration, 3)));

	}

	public int calorie(double OG, double FG, String unit) {

		// TODO this is broken for metric
		double Calorie_from_alcohol = 1881.22 * FG * (OG - FG) / (1.775 - OG);
		double Calories_from_carbs = 3550.0 * FG * ((0.1808 * OG) + (0.8192 * FG) - 1.0004);

		Log.d("Calories From Alcohol", Double.toString(Calorie_from_alcohol));
		Log.d("Calories From carbs", Double.toString(Calories_from_carbs));
		Log.d("OG", Double.toString(OG));
		Log.d("FG", Double.toString(FG));

		// return a value appropriate for standard or metric
		if (unit.equals("0"))
			return (int) Calorie_from_alcohol + (int) Calories_from_carbs;
		else
			return (int) (((int) Calorie_from_alcohol + (int) Calories_from_carbs) * 0.929885624);
		// https://www.google.com/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=calories+per+12+floz+to+calories+per+330+ml&safe=off
		// source for this conversion
		// 1 calories per (12 floz) = 0.929885624 calories per (330 ml)

	}

	public int attenuation(double OG, double FG) {
		return (int) (((OG - FG) / (OG - 1)) * 100);
	}

	public double mashWaterNeeded(Double mashThickness, double grainBill) {
		return (mashThickness * grainBill) / 4.0;
	}

	public double spargeWaterNeeded(double totalWater, double mashWater) {
		return totalWater - mashWater;
	}

	public double strikeTempNeeded(double grainBill, double mashVolume, double mashThickness, double targetMashTemp, double grainTemp) {
		return ((((grainBill * 0.05) + mashVolume * 1) * targetMashTemp) - ((grainBill * 0.05) * grainTemp)) / mashVolume;
	}

	public double totalWaterNeeded(double evaporationPercentage, int boilTime, double shrinkagePercent, double batchVolume, double trubLoss, double mashTunLoss, double grainAbsorbtionPercentage, double grainBill) {

		// add these up to get total water needed
		double shrinkageFactor, evaporationLossFactor, kettleLoss, grainAbsorbtion;

		// evaporationPercentage is taken from constants (percentBoilOff)
		evaporationLossFactor = 1 - ((evaporationPercentage / 100) * boilTime / 60);
		// shrinkagePercent is taken from constants (wortShrinkage)
		shrinkageFactor = 1 - (shrinkagePercent / 100);
		// trubLoss is taken from trubLoss on the GUI
		kettleLoss = ((batchVolume + trubLoss) / shrinkageFactor) / evaporationLossFactor;
		// figure out how much water will be absorbed from the grains
		grainAbsorbtion = grainAbsorbtionPercentage * grainBill;

		return kettleLoss + mashTunLoss + grainAbsorbtion;
	}

	public double preBoilVolume(double totalWater, double grainBill, double grainAbsorptionRate, double equipmentLoss) {
		return totalWater - (grainBill * grainAbsorptionRate) - equipmentLoss;
	}

	// these numbers were pulled from google
	public double galToLiter(double gallon) {
		return gallon * 3.78541;
	}

	public double literToGal(double liter) {
		return liter * 0.264172;
	}

	public double poundToKilogram(double pound) {
		return pound * 0.453592;
	}

	public double kilogramToPound(double kilogram) {
		return kilogram * 2.20462;
	}

	public double galPerPoundToLiterPerKilo(double convert) {
		return convert * 8.3450445;
	}

	public double literPerKiloToGalPerPound(double convert) {
		return convert * 0.119826427;
	}

	public double literPerKiloToQuartPerPound(double convert) {
		return convert * 0.479305709;
	}

	@SuppressWarnings("deprecation")
	public void checkUpdate(SharedPreferences prefs, TextView tv, AlertDialog checkUpdate, View v) {

		currentVersion = prefs.getInt("currentVersion", -1);

		checkUpdate.setCancelable(false);
		checkUpdate.setTitle("THERE IS A NEW UPDATE");
		checkUpdate.setView(v);
		tv.setText(updates);

		// setButton. -3 = BUTTON_NEUTRAL, "OK" is the text
		checkUpdate.setButton(-3, "OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// dismiss the dialog after pressing OK
				dialog.dismiss();
			}
		});

		// show dialog if first time on current version
		if (version != currentVersion) {
			checkUpdate.show();
			updateVersion(version, myVersion, prefs);
		}
	}

	public void updateVersion(int version, String myVersion, SharedPreferences prefs) {
		SharedPreferences.Editor edit = prefs.edit();
		edit.putInt("currentVersion", version);
		edit.putString("myVersion", myVersion);
		edit.commit();
	}
}
