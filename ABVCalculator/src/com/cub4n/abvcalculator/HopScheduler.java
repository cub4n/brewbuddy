package com.cub4n.abvcalculator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class HopScheduler extends Fragment {

	// TODO
	/*
	 * * Create object to hold all event information
	 * 
	 * * Find a way to dynamically display event information
	 * 
	 * * Consider having a notification display with next brew item and time
	 *   till then
	 * 
	 * * Fix Refracometer tab wordwrap bullshit
	 */

	private Button start, reset, timerValue, addEvent;


	private boolean paused = false;
	private boolean justStarted = true;

	private EditText etBoilTime, eventString, eventTime;

	private double startTime = 0.0;

	private Handler customHandler = new Handler();
	private AlertDialog boilTimeDialog, resetDialog, disableAlarmDialog,eventDialog;
	private MediaPlayer mMediaPlayer;

	private double timeInMilliseconds = 0.0;
	private double timeSwapBuff = 0.0;
	private double updateTime = 0.0;
	private int boilTime = 60, datTimeDoe, currentEventForAlarm = 0, tempEventTime;
	private String tempEventString;
	
	//grab preference n shit
    SharedPreferences prefs;

	ArrayList<Event> events = new ArrayList<Event>();
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			// We have different layouts, and in one of them this
			// fragment's containing frame doesn't exist. The fragment
			// may still be created from its saved state, but there is
			// no reason to try to create its view hierarchy because it
			// won't be displayed. Note this is not needed -- we could
			// just run the code below, where we would create and return
			// the view hierarchy; it would just never be used.
			return null;
		}
		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

		View V = inflater.inflate(R.layout.activity_hop_scheduler, container, false);
		// create the dialog
		boilTimeDialog = new AlertDialog.Builder(getActivity()).create();
		resetDialog = new AlertDialog.Builder(getActivity()).create();
		disableAlarmDialog = new AlertDialog.Builder(getActivity()).create();
		eventDialog = new AlertDialog.Builder(getActivity()).create();
		
		addEvent = (Button) V.findViewById(R.id.bAddEvent);
		addEvent.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				//set view for addEventDialog
				View eventDialogView = getActivity().getLayoutInflater().inflate(R.layout.add_event_dialog, null);
				
				//set Characteristics for addEventDialog
				eventDialog.setTitle("Enter New Event");
				eventDialog.setView(eventDialogView);
				
				//set click listener for event dialog
				eventDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								
								//set the views for the editText
								eventString = (EditText) eventDialog.findViewById(R.id.etEventName);
								eventTime = (EditText) eventDialog.findViewById(R.id.etEventBoilTime);
								
								try{
									// grab data from editText's and create the new event object
									tempEventString = eventString.getText().toString();
									tempEventTime = Integer.parseInt(eventTime.getText().toString());
									
									//check to see if eventString is empty
									if(tempEventString.equals("")){
										Toast.makeText(getActivity(), "Please enter in a valid event.",Toast.LENGTH_LONG).show();
									}else{
										//create a temp event
										Event tempEvent = new Event(tempEventString, tempEventTime);
									
										// add the temp event to the array list and sort them based on boil time
										events.add(tempEvent);
										Collections.sort(events);
										
										//clean up the dialog so next time it pops up its not populated with data from the last event
										if(eventString.length() > 0 && eventTime.length() > 0){ 
											//this line above gets rid of a warning when trying to do too much at once with the edit text
											eventString.setText("");
											eventTime.setText("");
										}
										
										//dismiss the dialog
										eventDialog.dismiss();
									}//end else
								} catch(NumberFormatException e){ //this is bad. but i dont know what exception is being thrown here.
									Toast.makeText(getActivity(), "Please enter in a valid event.",Toast.LENGTH_LONG).show();
								}
							}

								
						});
				eventDialog.show();
			}
		});

		// timer button stuff
		timerValue = (Button) V.findViewById(R.id.bTimerValue);
		timerValue.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				// dialogs n shit
				// open the layout for the dialog
				View boilDialogView = getActivity().getLayoutInflater().inflate(R.layout.boil_time_dialog, null);

				// set attributes for boilTimeDialog------------------------------------------------------
				boilTimeDialog.setTitle("Change Time");
				boilTimeDialog.setView(boilDialogView);

				boilTimeDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// initialize the edit text in the dialog
						etBoilTime = (EditText) boilTimeDialog.findViewById(R.id.etBoilTime);
						try {
							int tempBoilTime;

							// parse the value while checking if user enters in 0 for a boil time
							tempBoilTime = Integer.parseInt(etBoilTime.getText().toString());

							//check if user enters a valid boil time between 1 minute and 24 hours
							if (tempBoilTime > 0 && tempBoilTime < 1440) {
								boilTime = tempBoilTime;
								// change the button text
								timerValue.setText(Integer.toString(boilTime) + ":00");
							} else
								Toast.makeText(getActivity(), tempBoilTime + " minutes is not a valid time",Toast.LENGTH_LONG).show();

						}
						// if values are empty or invalid spit out a toast message and reset text views
						catch (NumberFormatException e) {
							Toast.makeText(getActivity(),"Time Not Changed",Toast.LENGTH_SHORT).show();
						}
						// dismiss the dialog after pressing OK
						dialog.dismiss();
					}
				});
				boilTimeDialog.show();
			}
		});// end onclicklistener

		// start button n stuff--------------------------------------------------------------------------
		start = (Button) V.findViewById(R.id.bStart);
		start.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//if the timer was running, set to paused, or timer just started
				if (paused == true || justStarted == true) {
					startTime = SystemClock.uptimeMillis();
					customHandler.postDelayed(updateTimerThread, 0);
					start.setText("Pause");
					paused = false;
					justStarted = false;
					timerValue.setClickable(false);
					
					//if it was paused, resume the timer
				} else if (paused == false) {
					timeSwapBuff += timeInMilliseconds;
					customHandler.removeCallbacks(updateTimerThread);
					start.setText("Resume");
					paused = true;
				}
			}
		});

		// reset button n shit----------------------------------------------------------------------------
		reset = (Button) V.findViewById(R.id.bReset);
		reset.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				//get layout for reset dialog
				View resetDialogView = getActivity().getLayoutInflater().inflate(R.layout.reset_dialog, null);
				
				resetDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener(){

					@Override
					// if the user wants to reset the timer, reset all the values associated
					public void onClick(DialogInterface arg0, int arg1) {
						timeInMilliseconds = 0.0;
						timeSwapBuff = 0.0;
						updateTime = 0.0;
						justStarted = true;
						timerValue.setClickable(true);
						start.setText("Start");
						customHandler.removeCallbacks(updateTimerThread);
						timerValue.setText(Integer.toString(boilTime) + ":00");
						currentEventForAlarm = 0;
						
						Toast.makeText(getActivity(),"Timer Reset",Toast.LENGTH_SHORT).show();
						
						resetDialog.dismiss();
					}
					
				});
				
				//if the user doesnt want to reset the timer and accidently hit the reset button
				resetDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) { 
						Toast.makeText(getActivity(),"Timer NOT Reset",Toast.LENGTH_SHORT).show(); 
						resetDialog.dismiss(); 
					}
				});
				
				// set attributes for dialog
				resetDialog.setCancelable(false);
				resetDialog.setTitle("Reset Timer?");
				resetDialog.setView(resetDialogView);

				resetDialog.show();
			}
		});

		//gay line of code that fragments need to prove theres an options menu n shit
		setHasOptionsMenu(true);
		return V;
	}//end of oncreate

	private Runnable updateTimerThread = new Runnable() {
		public void run() {

			// convert boil time to milliseconds
			datTimeDoe = boilTime * 60000;

			// subtract current time from start time to get a count down timer effect
			timeInMilliseconds = startTime - SystemClock.uptimeMillis();

			// add brew time, to the time counting down, plus down time from pausing timer
			updateTime = datTimeDoe + timeInMilliseconds + timeSwapBuff;

			// parse time to minutes and seconds
			int secs = (int) (updateTime / 1000);
			int mins = secs / 60;
			secs = secs % 60;

			// print out to display
			timerValue.setText("" + String.format("%02d", mins) + ":" + String.format("%02d", secs));

			//check to see if there are any more events for an alarm
			//also get the time for the current event if it exists and sound alarm when times match
			if(currentEventForAlarm < events.size() && events.get(currentEventForAlarm).getTime() == mins && secs == 0){
				//SOUND THE ALARM THE RED COATS ARE COMING
				alarm();
				//display the alarm dialog for the user to dismiss
				dismissAlarmDialog(events.get(currentEventForAlarm).getEventTitle());
				//move to the next event in the array list
				currentEventForAlarm++;
			}
			
			// lets do this again in 1 second. (saves battery not doing so many computations.
			customHandler.postDelayed(this, 1000);
			
			//if the timer has run to 0 stop the thread
			if(mins == 0 && secs == 0){
				alarm();
				dismissAlarmDialog("Timer Complete");
				customHandler.removeCallbacks(updateTimerThread);
			}
		}
	};
	
	private void alarm(){
		//grab value for the alarm tone, if not previously set (on first time run) defaults to the default notification sound.
	    String alarms = prefs.getString("alarm_tone", "content://settings/system/notification_sound");
	    Uri uri = Uri.parse(alarms);
	    playSound(getActivity(), uri);

	    //call mMediaPlayer.stop(); when you want the sound to stop
	}


	//create the media player object that plays the notification sound.
	private void playSound(Context context, Uri alert) {
	        mMediaPlayer = new MediaPlayer();
	        try {
	            mMediaPlayer.setDataSource(context, alert);
	            final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
	            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
	                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
	                mMediaPlayer.prepare();
	                mMediaPlayer.setLooping(true);
	                mMediaPlayer.start();
	            }
	        } catch (IOException e) {
	            System.out.println("Problem playing alarm sound.");
	        }
	    }
	
	//method used to create dialog for dismissing the alarm.
	private void dismissAlarmDialog(String eventTitle){
		View dismissAlarmDialogView = getActivity().getLayoutInflater().inflate(R.layout.dismiss_alarm_dialog, null);
		
		disableAlarmDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						//on button press for disable alarm, stop the media player, and dismiss the dialog
						mMediaPlayer.stop();
						disableAlarmDialog.dismiss();
					}
				});
		
		//set characteristics for dialog
		disableAlarmDialog.setCancelable(false);
		disableAlarmDialog.setTitle(eventTitle);
		disableAlarmDialog.setView(dismissAlarmDialogView);

		disableAlarmDialog.show();
	}
	
	// sets menu entries from optionsmenu.xml in the menu directory
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// Inflate the menu; this adds items to the action bar if it is present. 
		inflater.inflate(R.menu.optionsmenu, menu);
	}

	// assigns actions for each menu entry
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case R.id.preferences:
			intent = new Intent(this.getActivity(), Prefs.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;

		case R.id.information:
			intent = new Intent(this.getActivity(), Info.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		case R.id.changelog:
			intent = new Intent(this.getActivity(), Changelog.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
		return super.onOptionsItemSelected(item);
	}
	}