package com.cub4n.abvcalculator;

import java.text.DecimalFormat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Hydrometer extends Fragment implements OnClickListener {

	// initialize all variables, and aspects for the GUI
	double percentAlcohol, alcoholByWeight, OG, FG, correction, temp, temp2,
			calibration;
	boolean theUnit = false, ogFlag, fgFlag;
	int caloriesPerBeer, attentuation;

	AlertDialog checkUpdateDialog;

	Button calculate;
	EditText initialGravity, finalGravity, temperature, temperature2,
			calibrate;
	TextView ABV, ABW, adjustedInitialGravity, adjustedFinalGravity, temp1Unit,
			temp2Unit, calibrationUnit, calories, updateTextDialog, atten;
	DecimalFormat oneDecimal = new DecimalFormat("#.#");
	DecimalFormat gravity = new DecimalFormat("#.###");
	String unit, calibratedHydrometerTemp, activityCheck;
	SharedPreferences prefs;

	// create object for to use formulas
	Formulas formula = new Formulas();

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			// We have different layouts, and in one of them this
			// fragment's containing frame doesn't exist. The fragment
			// may still be created from its saved state, but there is
			// no reason to try to create its view hierarchy because it
			// won't be displayed. Note this is not needed -- we could
			// just run the code below, where we would create and return
			// the view hierarchy; it would just never be used.
			return null;
		}

		// Inflate the Fragments View
		View V = inflater.inflate(R.layout.activity_hydrometer, container, false);

		// sets up the view to hide the keyboard on touch events that dont focus
		// on an editText
		setupUI(V);

		// pass through the inflater, and container to use for the
		// findViewById()
		initialize(V);
		setHasOptionsMenu(true);
		return V;
	}

	private void initialize(View V) {
		// initialize dialog for change text dialog n shit
		checkUpdateDialog = new AlertDialog.Builder(getActivity()).create();

		View updateDialogView = getActivity().getLayoutInflater().inflate(R.layout.check_update_dialog, null);

		updateTextDialog = (TextView) updateDialogView.findViewById(R.id.tvUpdateText);

		// grab preferences
		prefs = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

		// checks version for update
		formula.checkUpdate(prefs, updateTextDialog, checkUpdateDialog, updateDialogView);

		// set onClickListeners
		calculate = (Button) V.findViewById(R.id.bCalculate);
		calculate.setOnClickListener(this);

		// initialize buttons and textviews, then set their ID's to the xml
		initialGravity = (EditText) V.findViewById(R.id.etInitialGravity);
		finalGravity = (EditText) V.findViewById(R.id.etFinalGravity);
		temperature = (EditText) V.findViewById(R.id.etTemp);
		temperature2 = (EditText) V.findViewById(R.id.etTemp2);
		calibrate = (EditText) V.findViewById(R.id.etCalibration);
		adjustedInitialGravity = (TextView) V.findViewById(R.id.tvAdjustedOriginalGravity);
		adjustedFinalGravity = (TextView) V.findViewById(R.id.tvAdjustedFinalGravity);
		ABV = (TextView) V.findViewById(R.id.tvABV);
		ABW = (TextView) V.findViewById(R.id.tvABW);
		temp1Unit = (TextView) V.findViewById(R.id.tvTempUnit);
		temp2Unit = (TextView) V.findViewById(R.id.tvTemp2Unit);
		calibrationUnit = (TextView) V.findViewById(R.id.tvCalibrationUnit);
		calories = (TextView) V.findViewById(R.id.tvCaloriesPerBeer);
		atten = (TextView) V.findViewById(R.id.tvAttentuation);

		// updates text based on preferences
		unit = prefs.getString("unit", "");
		calibratedHydrometerTemp = prefs.getString("calibrationTemp", "");
		if (unit.equals("0")) {
			temp1Unit.setText("F");
			temp2Unit.setText("F");
			calibrationUnit.setText("F");
			temperature.setText("" + calibratedHydrometerTemp);
			temperature2.setText("" + calibratedHydrometerTemp);
			calibrate.setText("" + calibratedHydrometerTemp);
			calories.setText("Calories Per Beer (12oz): ");

		} else if (unit.equals("1")) {
			temp1Unit.setText("C");
			temp2Unit.setText("C");
			calibrationUnit.setText("C");
			temperature.setText("" + calibratedHydrometerTemp);
			temperature2.setText("" + calibratedHydrometerTemp);
			calibrate.setText("" + calibratedHydrometerTemp);
			calories.setText("Calories Per Beer (330ml): ");
		}
	}

	// when the calculate button has been pressed
	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		// if the calculate button is pressed
		case R.id.bCalculate:

			// reset flags to see if values have been entered in for OG and FG
			// false means the editText remains empty
			OG = 0.0;
			ogFlag = false;

			FG = 0.0;
			fgFlag = false;

			Log.d("ogFlag", Boolean.toString(ogFlag));
			Log.d("fgFlag", Boolean.toString(fgFlag));

			// if tempurature units are in Celsius convert to Fahrenheit
			unit = prefs.getString("unit", "");
			Log.d("Unit", unit);
			// fahrenheit: unit = 0; Celcius: unit = 1
			if (unit.equals("1")) {
				temp = formula.CtoF(temp);
				temp2 = formula.CtoF(temp2);
				calibration = formula.CtoF(calibration);
			}

			// nested try catch to check values for OG and FG checks if data was
			// put into the text views for initial and final gravity
			try {
				try {
					Log.d("OG Before", Double.toString(OG));
					OG = Double.parseDouble(initialGravity.getText().toString());
					Log.d("OG After", Double.toString(OG));
				} finally {
					// if this check passes, a value was entered into OG
					if (OG != 0.0) {
						ogFlag = true;
					}// end if

					// check if data was put into fg also
					try {
						Log.d("FG Before", Double.toString(FG));
						FG = Double.parseDouble(finalGravity.getText().toString());
						Log.d("FG After", Double.toString(FG));
					} finally {
						// if this check passes, a value was entered into FG
						if (FG != 0.0) {
							fgFlag = true;
						}
					}
				}// end finally
			} // end try
				// if values are empty or invalid spit out a toast message and
				// reset text views
			catch (NumberFormatException e) {

				if (ogFlag == false && fgFlag == false) {
					Toast.makeText(getActivity(), "Sorry you didn't type anything", Toast.LENGTH_SHORT).show();
					reset();
				}
				// after checks have been made, decide what you're going to do.
				// compute abv, or just convert values for og and/or fg
			} finally {
				// if both OG and FG have values
				if (ogFlag == true && fgFlag == true) {

					try {
						// grab the values entered into the tempurature text
						// views and calibration temp
						temp = Double.parseDouble(temperature.getText().toString());
						temp2 = Double.parseDouble(temperature2.getText().toString());
						calibration = Double.parseDouble(calibrate.getText().toString());
					} catch (NumberFormatException e) {
						Toast.makeText(getActivity(), "Please make sure there are values for the appropriate temperature readings.", Toast.LENGTH_LONG).show();
					}

					// calculate hydrometer calibration correction
					OG = formula.calculation(OG, temp, calibration);
					FG = formula.calculation(FG, temp2, calibration);

					// calculate values
					percentAlcohol = ((1.05 / .79) * ((OG - FG) / FG) * 100);
					alcoholByWeight = (.79 * percentAlcohol) / FG;

					// pass through OG, FG, and the unit specified by the user
					// unit == 0; F
					// unit == 1; C
					caloriesPerBeer = formula.calorie(OG, FG, unit);

					// calculate attenuation
					attentuation = formula.attenuation(OG, FG);

					// update text views
					ABV.setText("Calculated ABV: " + oneDecimal.format(percentAlcohol) + "%");
					adjustedInitialGravity.setText("Adjusted Original Gravity: " + gravity.format(OG));
					adjustedFinalGravity.setText("Adjusted Final Gravity: " + gravity.format(FG));
					ABW.setText("Calculated ABW: " + oneDecimal.format(alcoholByWeight) + "%");
					atten.setText("Attentuation: " + attentuation + "%");

					Log.d("unit", unit.toString());

					if (unit.equals("0"))
						calories.setText("Calories Per Beer (12oz): " + caloriesPerBeer);
					else if (unit.equals("1"))
						calories.setText("Calories Per Beer (330ml): " + caloriesPerBeer);

					// if tempurature units are in Fahrenheit but originally in
					// Celsius convert back to Celsius for consistancy
					// all calulations have been computed already.
					// for example user is using C, the algorithm switches to F
					// to compute then back to C to display to the user.
					if (unit.equals("1")) {
						temp = formula.FtoC(temp);
						temp2 = formula.FtoC(temp2);
						calibration = formula.FtoC(calibration);
					}// end if
					// if a value was entered in og and not fg
				} else if (ogFlag == true && fgFlag == false) {
					try {
						// grab the values entered into the tempurature text
						// views and calibration temp
						temp = Double.parseDouble(temperature.getText().toString());
						calibration = Double.parseDouble(calibrate.getText().toString());

						OG = formula.calculation(OG, temp, calibration);
						reset();
						adjustedInitialGravity.setText("Adjusted Original Gravity: " + gravity.format(OG));
					} catch (NumberFormatException e) {
						Toast.makeText(getActivity(), "Please make sure there are values for the appropriate temperature readings.", Toast.LENGTH_LONG).show();
						reset();
					}

					// if a value was entered in fg and not og
				} else if (ogFlag == false && fgFlag == true) {

					try {
						// grab the values entered into the tempurature text
						// views and calibration temp
						temp2 = Double.parseDouble(temperature2.getText().toString());
						calibration = Double.parseDouble(calibrate.getText().toString());

						FG = formula.calculation(FG, temp2, calibration);
						reset();
						adjustedFinalGravity.setText("Adjusted Final Gravity: " + gravity.format(FG));
					} catch (NumberFormatException e) {
						Toast.makeText(getActivity(), "Please make sure there are values for the appropriate temperature readings.", Toast.LENGTH_LONG).show();
						reset();
					}

					// if no values were entered but numbers were previously
					// entered
				} else {
					Toast.makeText(getActivity(), "Please enter in values for OG and/or FG", Toast.LENGTH_LONG).show();
					reset();
				}
			}
			break;
		}
	};

	public void reset() {
		ABV.setText("Calculated ABV: 0.0%");
		adjustedInitialGravity.setText("Adjusted Original Gravity: 0.000");
		adjustedFinalGravity.setText("Adjusted Final Gravity: 0.000");
		ABW.setText("Calculated ABW: 0.0%");
		if (unit.equals("0")) {
			temp1Unit.setText("F");
			temp2Unit.setText("F");
			calibrationUnit.setText("F");
			calories.setText("Calories Per Beer (12oz): 0");
		} else if (unit.equals("1")) {
			temp1Unit.setText("C");
			temp2Unit.setText("C");
			calibrationUnit.setText("C");
			calories.setText("Calories Per Beer (330ml): 0");
		}
	}

	// sets menu entries from optionsmenu.xml in the menu directory
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// Inflate the menu; this adds items to the action bar if it is present.
		inflater.inflate(R.menu.optionsmenu, menu);
	}

	// assigns actions for each menu entry
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case R.id.preferences:
			intent = new Intent(this.getActivity(), Prefs.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;

		case R.id.information:
			intent = new Intent(this.getActivity(), Info.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		case R.id.changelog:
			intent = new Intent(this.getActivity(), Changelog.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		case R.id.contact:
			Intent email = new Intent(Intent.ACTION_SEND);
			email.putExtra(Intent.EXTRA_EMAIL, new String[] { "brewbuddydev@gmail.com" });
			email.putExtra(Intent.EXTRA_SUBJECT, "Brew Buddy: 'add subject here'");
			email.setType("message/rfc822");
			startActivity(Intent.createChooser(email, "Choose an Email client :"));
			break;
		// case R.id.donate:
		// intent = new Intent(this.getActivity(), .class);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// startActivity(intent);
		// break;

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		unit = prefs.getString("unit", "");
		calibratedHydrometerTemp = prefs.getString("calibrationTemp", "");

		reset();
	}

	public static void hideSoftKeyboard(Activity activity) {
		// hide keyboard after button pressed
		InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public void setupUI(View view) {

		// Set up touch listener for non-text box views to hide keyboard.
		if (!(view instanceof EditText)) {
			view.setOnTouchListener(new OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
					hideSoftKeyboard(getActivity());
					return false;
				}
			});
		}

		// If a layout container, iterate over children and seed recursion.
		if (view instanceof ViewGroup) {

			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
				View innerView = ((ViewGroup) view).getChildAt(i);
				setupUI(innerView);
			}
		}
	}

}
