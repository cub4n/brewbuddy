package com.cub4n.abvcalculator;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Info extends Activity implements OnClickListener{
	
	Button linkToSeanTerrill;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);
		
		linkToSeanTerrill = (Button) findViewById(R.id.bSeanTerrill);
		linkToSeanTerrill.setOnClickListener(this);
		
		int deviceVersion = android.os.Build.VERSION.SDK_INT;
		Log.d("version", Integer.toString(deviceVersion));
		if (deviceVersion > 10) {
			ActionBar bar = getActionBar();
			ColorDrawable myColor = new ColorDrawable( Color.parseColor("#825A36"));
			bar.setBackgroundDrawable(myColor);
			// bar.setBackgroundDrawable(new
			// ColorDrawable(Color.parseColor("#603814")));
		}	
		
	}
	
	@Override
	public void onClick(View v){
		switch(v.getId()){
		case R.id.bSeanTerrill:
			goToUrl("http://seanterrill.com/2012/01/06/refractometer-calculator/");
			break;
		}
	}
	
	public void goToUrl(String url){
		Uri uriUrl = Uri.parse(url);
		Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
		startActivity(launchBrowser);
	}


}
