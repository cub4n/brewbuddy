package com.cub4n.abvcalculator;

import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Mash extends Fragment implements OnClickListener {

	SharedPreferences prefs;

	// initialize buttons/textviews
	EditText batchSizeet, grainBillet, boilTimeet, trubLosset, equipmentLosset,
			mashThicknesset, grainTempet, targetMashTempet, wortShrinkageet,
			grainAbsorptionConstantet, percentBoilOffet;
	Button calculate, mashHelp;
	TextView totalWaterNeededtv, mashWaterNeededtv, spargeWaterNeededtv,
			strikeWaterTemptv, preBoilVolumetv;

	TextView batchSizetv, grainBilltv, boilTimetv, trubLosstv, equipmentLosstv,
			mashThicknesstv, grainTemptv, targetMashTemptv,
			grainAbsorbtionConstanttv;

	Formulas formula;
	DecimalFormat formatter = new DecimalFormat("#.###");

	int boilTime, wortShrinkage, percentBoilOff;
	double batchSize, grainBill, trubLoss, equipmentLoss, mashThickness,
			grainTemp, targetMashTemp, grainAbsorptionConstant,
			totalWaterNeeded, mashWaterNeeded, spargeWaterNeeded, strikeTemp,
			preBoilWortProduced, preBoilVolume;

	String unit;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			// We have different layouts, and in one of them this
			// fragment's containing frame doesn't exist. The fragment
			// may still be created from its saved state, but there is
			// no reason to try to create its view hierarchy because it
			// won't be displayed. Note this is not needed -- we could
			// just run the code below, where we would create and return
			// the view hierarchy; it would just never be used.
			return null;
		}

		// Inflate the Fragments View
		View V = inflater.inflate(R.layout.activity_mash, container, false);

		// sets up the view to hide the keyboard on touch events that dont focus
		// on an editText
		setupUI(V);

		// pass through the inflater, and container to use for the
		// findViewById()
		initialize(V);
		setHasOptionsMenu(true);

		return V;
	}

	private void initialize(View V) {
		formula = new Formulas();

		// grab preferences
		prefs = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

		// initialize buttons/textviews
		batchSizeet = (EditText) V.findViewById(R.id.etBatchSize);
		grainBillet = (EditText) V.findViewById(R.id.etGrainBill);
		boilTimeet = (EditText) V.findViewById(R.id.etBoilTime);
		trubLosset = (EditText) V.findViewById(R.id.etTrubLoss);
		equipmentLosset = (EditText) V.findViewById(R.id.etEquipmentLoss);
		mashThicknesset = (EditText) V.findViewById(R.id.etMashThickness);
		grainTempet = (EditText) V.findViewById(R.id.etGrainTemperature);
		targetMashTempet = (EditText) V.findViewById(R.id.etTargetMashTemp);
		wortShrinkageet = (EditText) V.findViewById(R.id.etWortShrinkage);
		grainAbsorptionConstantet = (EditText) V.findViewById(R.id.etGrainAbsorptionConstant);
		percentBoilOffet = (EditText) V.findViewById(R.id.etPercentBoilOff);

		calculate = (Button) V.findViewById(R.id.bCalculate);
		calculate.setOnClickListener(this);

		mashHelp = (Button) V.findViewById(R.id.bMashHelp);
		mashHelp.setOnClickListener(this);

		totalWaterNeededtv = (TextView) V.findViewById(R.id.tvTotalWaterNeeded);
		mashWaterNeededtv = (TextView) V.findViewById(R.id.tvMashWaterNeeded);
		spargeWaterNeededtv = (TextView) V.findViewById(R.id.tvSpargeWaterNeeded);
		strikeWaterTemptv = (TextView) V.findViewById(R.id.tvStrikeWaterTemp);

		batchSizetv = (TextView) V.findViewById(R.id.tvBatchSize);
		grainBilltv = (TextView) V.findViewById(R.id.tvGrainBill);
		boilTimetv = (TextView) V.findViewById(R.id.tvBoilTime);
		trubLosstv = (TextView) V.findViewById(R.id.tvTrubLoss);
		equipmentLosstv = (TextView) V.findViewById(R.id.tvEquipmentLoss);
		mashThicknesstv = (TextView) V.findViewById(R.id.tvMashThickness);
		grainTemptv = (TextView) V.findViewById(R.id.tvGrainTemperature);
		targetMashTemptv = (TextView) V.findViewById(R.id.tvTargetMashTemp);
		grainAbsorbtionConstanttv = (TextView) V.findViewById(R.id.tvGrainAbsorptionConstant);
		preBoilVolumetv = (TextView) V.findViewById(R.id.tvPreBoilVolume);

		// updates text based on preferences
		unit = prefs.getString("unit", "");

		// created a preference and assign the previous unit to the current unit
		// to initialize. this will be
		// useful for when the app is stopped and started. this way, we can
		// track when to convert grain
		// absorption rate

		SharedPreferences.Editor edit = prefs.edit();
		edit.putString("previousUnit", unit);
		edit.commit();

		if (unit.equals("0")) {
			batchSizetv.setText("Batch Size (Gal) ");
			grainBilltv.setText("Grain Bill (Lbs) ");
			trubLosstv.setText("Trub Loss (Gal) ");
			equipmentLosstv.setText("Equipment Loss (Gal) ");
			mashThicknesstv.setText("Mash Thickness (Qts/lb) ");
			grainTemptv.setText("Grain Temp (F) ");
			targetMashTemptv.setText("Target Mash Temp (F) ");
			grainAbsorbtionConstanttv.setText("Grain Absorption\n Constant (Gal/lb) ");
			totalWaterNeededtv.setText("Total Water Needed (Gal): ");
			mashWaterNeededtv.setText("Mash Water Needed (Gal): ");
			spargeWaterNeededtv.setText("Sparge Water Needed (Gal): ");
			strikeWaterTemptv.setText("Strike Temp (F): ");
			preBoilVolumetv.setText("Pre-Boil Wort Volume (Gal): ");
			
			batchSizeet.setText("5");
			grainBillet.setText("10");
			boilTimeet.setText("60");
			trubLosset.setText(".5");
			equipmentLosset.setText("1");
			mashThicknesset.setText("1.33");
			grainTempet.setText("70");
			targetMashTempet.setText("152");
			grainAbsorptionConstantet.setText("0.13");
			
		} else if (unit.equals("1")) {
			batchSizetv.setText("Batch Size (Litre) ");
			grainBilltv.setText("Grain Bill (Kg) ");
			trubLosstv.setText("Trub Loss (Litre) ");
			equipmentLosstv.setText("Equipment Loss (Litre) ");
			mashThicknesstv.setText("Mash Thickness (Litre/Kg) ");
			grainTemptv.setText("Grain Temp (C) ");
			targetMashTemptv.setText("Target Mash Temp (C) ");
			grainAbsorbtionConstanttv.setText("Grain Absorption\n Constant (Litre/Kg) ");
			grainAbsorptionConstantet.setText("1.082");
			totalWaterNeededtv.setText("Total Water Needed (Litre): ");
			mashWaterNeededtv.setText("Mash Water Needed (Litre): ");
			spargeWaterNeededtv.setText("Sparge Water Needed (Litre): ");
			strikeWaterTemptv.setText("Strike Temp (C): ");
			preBoilVolumetv.setText("Pre-Boil Wort Volume (Litre): ");
			
			batchSizeet.setText("19");
			grainBillet.setText("4.5");
			boilTimeet.setText("60");
			trubLosset.setText("1.9");
			equipmentLosset.setText("3.8");
			mashThicknesset.setText("2.77");
			grainTempet.setText("21.1");
			targetMashTempet.setText("66.6");
			grainAbsorptionConstantet.setText("1.082");
		}

	}

	public void onClick(View v) {

		if (v.getId() == R.id.bCalculate) {
			
			InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

			unit = prefs.getString("unit", "");
			try {
				// get values from user. All values are required for calculations
				batchSize = Double.parseDouble(batchSizeet.getText().toString());
				grainBill = Double.parseDouble(grainBillet.getText().toString());
				boilTime = Integer.parseInt(boilTimeet.getText().toString());
				trubLoss = Double.parseDouble(trubLosset.getText().toString());
				equipmentLoss = Double.parseDouble(equipmentLosset.getText().toString());
				mashThickness = Double.parseDouble(mashThicknesset.getText().toString());
				grainTemp = Double.parseDouble(grainTempet.getText().toString());
				targetMashTemp = Double.parseDouble(targetMashTempet.getText().toString());
				wortShrinkage = Integer.parseInt(wortShrinkageet.getText().toString());
				grainAbsorptionConstant = Double.parseDouble(grainAbsorptionConstantet.getText().toString());
				percentBoilOff = Integer.parseInt(percentBoilOffet.getText().toString());

				if (unit.equals("0")) {
					// calculate values. no conversion needed because its already in standard units.
					totalWaterNeeded = formula.totalWaterNeeded(percentBoilOff, boilTime, wortShrinkage, batchSize, trubLoss, equipmentLoss, grainAbsorptionConstant, grainBill);
					mashWaterNeeded = formula.mashWaterNeeded(mashThickness, grainBill);
					spargeWaterNeeded = formula.spargeWaterNeeded(totalWaterNeeded, mashWaterNeeded);
					strikeTemp = formula.strikeTempNeeded(grainBill, mashWaterNeeded, mashThickness, targetMashTemp, grainTemp);
					preBoilVolume = formula.preBoilVolume(totalWaterNeeded, grainBill, grainAbsorptionConstant, equipmentLoss);

					totalWaterNeededtv.setText("Total Water Needed (Gal): " + formatter.format(totalWaterNeeded));
					mashWaterNeededtv.setText("Mash Water Needed (Gal): " + formatter.format(mashWaterNeeded));
					spargeWaterNeededtv.setText("Sparge Water Needed (Gal): " + formatter.format(spargeWaterNeeded));
					strikeWaterTemptv.setText("Strike Temp (F): " + formatter.format(strikeTemp));
					preBoilVolumetv.setText("Pre-Boil Wort Volume (Gal): " + formatter.format(preBoilVolume));
				} else if (unit.equals("1")) {

					// convert from metric to standard
					batchSize = formula.literToGal(batchSize);
					Log.d("batchSize", Double.toString(batchSize));
					grainBill = formula.kilogramToPound(grainBill);
					Log.d("Grain Bill", Double.toString(grainBill));
					trubLoss = formula.literToGal(trubLoss);
					Log.d("trubLoss", Double.toString(trubLoss));
					equipmentLoss = formula.literToGal(equipmentLoss);
					Log.d("equipmentLoss", Double.toString(equipmentLoss));
					mashThickness = formula.literPerKiloToQuartPerPound(mashThickness);
					Log.d("mashThickness", Double.toString(mashThickness));
					grainTemp = formula.CtoF(grainTemp);
					Log.d("grainTemp", Double.toString(grainTemp));
					targetMashTemp = formula.CtoF(targetMashTemp);
					Log.d("TargetMashTemp", Double.toString(targetMashTemp));
					grainAbsorptionConstant = formula.literPerKiloToGalPerPound(grainAbsorptionConstant);
					Log.d("grainAbsorptionConstant", Double.toString(grainAbsorptionConstant));

					// calculate shit
					totalWaterNeeded = formula.totalWaterNeeded(percentBoilOff, boilTime, wortShrinkage, batchSize, trubLoss, equipmentLoss, grainAbsorptionConstant, grainBill);
					mashWaterNeeded = formula.mashWaterNeeded(mashThickness, grainBill);
					spargeWaterNeeded = formula.spargeWaterNeeded(totalWaterNeeded, mashWaterNeeded);
					strikeTemp = formula.strikeTempNeeded(grainBill, mashWaterNeeded, mashThickness, targetMashTemp, grainTemp);
					preBoilVolume = formula.preBoilVolume(totalWaterNeeded, grainBill, grainAbsorptionConstant, equipmentLoss);

					// convert back to metric to display to user
					totalWaterNeeded = formula.galToLiter(totalWaterNeeded);
					mashWaterNeeded = formula.galToLiter(mashWaterNeeded);
					spargeWaterNeeded = formula.galToLiter(spargeWaterNeeded);
					strikeTemp = formula.FtoC(strikeTemp);
					preBoilVolume = formula.galToLiter(preBoilVolume);

					// display to user
					totalWaterNeededtv.setText("Total Water Needed (Litre): " + formatter.format(totalWaterNeeded));
					mashWaterNeededtv.setText("Mash Water Needed (Litre): " + formatter.format(mashWaterNeeded));
					spargeWaterNeededtv.setText("Sparge Water Needed (Litre): " + formatter.format(spargeWaterNeeded));
					strikeWaterTemptv.setText("Strike Temp (C): " + formatter.format(strikeTemp));
					preBoilVolumetv.setText("Pre-Boil Wort Volume (Litre): " + formatter.format(preBoilVolume));

				}

			} catch (NumberFormatException e) {
				Toast.makeText(getActivity(), "Some Data Values Are Missing.", Toast.LENGTH_SHORT).show();
			}

		} else if (v.getId() == R.id.bMashHelp) {
			Intent mashHelpActivity = new Intent(getActivity(), MashHelp.class);
			startActivity(mashHelpActivity);

		}
	}

	public void setupUI(View view) {

		// Set up touch listener for non-text box views to hide keyboard.
		if (!(view instanceof EditText)) {
			view.setOnTouchListener(new OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
//					hideSoftKeyboard(getActivity());
					return false;
				}
			});
		}

		// If a layout container, iterate over children and seed recursion.
		if (view instanceof ViewGroup) {

			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
				View innerView = ((ViewGroup) view).getChildAt(i);
				setupUI(innerView);
			}
		}
	}

//	public static void hideSoftKeyboard(Activity activity) {
//		// hide keyboard after button pressed
//		InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
//		inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//	}

	// sets menu entries from optionsmenu.xml in the menu directory
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// Inflate the menu; this adds items to the action bar if it is present.

		inflater.inflate(R.menu.optionsmenu, menu);
	}

	// assigns actions for each menu entry
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case R.id.preferences:
			intent = new Intent(this.getActivity(), Prefs.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;

		case R.id.information:
			intent = new Intent(this.getActivity(), Info.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		case R.id.changelog:
			intent = new Intent(this.getActivity(), Changelog.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		case R.id.contact:
			Intent email = new Intent(Intent.ACTION_SEND);
			email.putExtra(Intent.EXTRA_EMAIL, new String[] { "brewbuddydev@gmail.com" });
			email.putExtra(Intent.EXTRA_SUBJECT, "Brew Buddy: 'add subject here'");
			email.setType("message/rfc822");
			startActivity(Intent.createChooser(email, "Choose an Email client :"));
			break;
		// case R.id.donate:
		// intent = new Intent(this.getActivity(), .class);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// startActivity(intent);
		// break;

		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onPause(){
		super.onPause();
		
		try{
		// get values from user. All values are required for calculations
		batchSize = Double.parseDouble(batchSizeet.getText().toString());
		grainBill = Double.parseDouble(grainBillet.getText().toString());
		boilTime = Integer.parseInt(boilTimeet.getText().toString());
		trubLoss = Double.parseDouble(trubLosset.getText().toString());
		equipmentLoss = Double.parseDouble(equipmentLosset.getText().toString());
		mashThickness = Double.parseDouble(mashThicknesset.getText().toString());
		grainTemp = Double.parseDouble(grainTempet.getText().toString());
		targetMashTemp = Double.parseDouble(targetMashTempet.getText().toString());
		wortShrinkage = Integer.parseInt(wortShrinkageet.getText().toString());
		grainAbsorptionConstant = Double.parseDouble(grainAbsorptionConstantet.getText().toString());
		percentBoilOff = Integer.parseInt(percentBoilOffet.getText().toString());
		
		SharedPreferences.Editor edit = prefs.edit();
		edit.putString("batchSize", Double.toString(batchSize));
		edit.putString("grainBill", Double.toString(grainBill));
		edit.putString("boilTime", Integer.toString(boilTime));
		edit.putString("trubLoss", Double.toString(trubLoss));
		edit.putString("equipmentLoss", Double.toString(equipmentLoss));
		edit.putString("mashThickness", Double.toString(mashThickness));
		edit.putString("grainTemp", Double.toString(grainTemp));
		edit.putString("targetMashTemp", Double.toString(targetMashTemp));
		edit.putString("wortShrinkage", Integer.toString(wortShrinkage));
		edit.putString("grainAbsorptionConstant", Double.toString(grainAbsorptionConstant));
		edit.putString("percentBoilOff", Integer.toString(percentBoilOff));
		
		edit.putString("previousUnit", unit);
		
		edit.commit();
		
		} catch(NumberFormatException e){
			//do nothing
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// updates text based on preferences
		unit = prefs.getString("unit", "");
		String previousUnit = prefs.getString("previousUnit", "");
				
		//there are 4 cass here
		// where unit = F and it wasnt just changed (used saved values)
		// where unit = C and it wasnt just changed (used saved values)
		// where unit = F and it was just changed (used default values)
		// where unit = C and it was just changed (used default values)
		if (unit.equals("0") && unit.equals(previousUnit)) {
			batchSizetv.setText("Batch Size (Gal) ");
			grainBilltv.setText("Grain Bill (Lbs) ");
			trubLosstv.setText("Trub Loss (Gal) ");
			equipmentLosstv.setText("Equipment Loss (Gal) ");
			mashThicknesstv.setText("Mash Thickness (Qts/lb) ");
			grainTemptv.setText("Grain Temp (F) ");
			targetMashTemptv.setText("Target Mash Temp (F) ");
			grainAbsorbtionConstanttv.setText("Grain Absorption\n Constant (Gal/lb) ");
			totalWaterNeededtv.setText("Total Water Needed (Gal): ");
			mashWaterNeededtv.setText("Mash Water Needed (Gal): ");
			spargeWaterNeededtv.setText("Sparge Water Needed (Gal): ");
			strikeWaterTemptv.setText("Strike Temp (F): ");
			preBoilVolumetv.setText("Pre-Boil Wort Volume (Gal): ");
			
			// if these values havent been set yet. set the default values using the second parameter
			String batchSize = prefs.getString("batchSize", "5");
			String grainBill = prefs.getString("grainBill", "10");
			String boilTime = prefs.getString("boilTime", "60");
			String trubLoss = prefs.getString("trubLoss", ".5");
			String equipmentLoss = prefs.getString("equipmentLoss", "1");
			String mashThickness = prefs.getString("mashThickness", "1.33");
			String grainTemp = prefs.getString("grainTemp", "70");
			String targetMashTemp = prefs.getString("targetMashTemp", "152");
			String wortShrinkage = prefs.getString("wortShrinkage", "4");
			String grainAbsorptionConstant = prefs.getString("grainAbsorptionConstant", "0.13");
			String percentBoilOff = prefs.getString("percentBoilOff", "10");			
			
			//set the text to the values directly above
			batchSizeet.setText(batchSize);
			grainBillet.setText(grainBill);
			boilTimeet.setText(boilTime);
			trubLosset.setText(trubLoss);
			equipmentLosset.setText(equipmentLoss);
			mashThicknesset.setText(mashThickness);
			grainTempet.setText(grainTemp);
			wortShrinkageet.setText(wortShrinkage);
			targetMashTempet.setText(targetMashTemp);
			grainAbsorptionConstantet.setText(grainAbsorptionConstant);
			percentBoilOffet.setText(percentBoilOff);
			
		} else if (unit.equals("1") && unit.equals(previousUnit)) {
			batchSizetv.setText("Batch Size (Litre) ");
			grainBilltv.setText("Grain Bill (Kg) ");
			trubLosstv.setText("Trub Loss (Litre) ");
			equipmentLosstv.setText("Equipment Loss (Litre) ");
			mashThicknesstv.setText("Mash Thickness (Litre/Kg) ");
			grainTemptv.setText("Grain Temp (C) ");
			targetMashTemptv.setText("Target Mash Temp (C) ");
			grainAbsorbtionConstanttv.setText("Grain Absorption\n Constant (Litre/Kg) ");
			grainAbsorptionConstantet.setText("1.082");
			totalWaterNeededtv.setText("Total Water Needed (Litre): ");
			mashWaterNeededtv.setText("Mash Water Needed (Litre): ");
			spargeWaterNeededtv.setText("Sparge Water Needed (Litre): ");
			strikeWaterTemptv.setText("Strike Temp (C): ");
			preBoilVolumetv.setText("Pre-Boil Wort Volume (Litre): ");
			
			// if these values havent been set yet. set the default values using the second parameter
			String batchSize = prefs.getString("batchSize", "19");
			String grainBill = prefs.getString("grainBill", "4.5");
			String boilTime = prefs.getString("boilTime", "60");
			String trubLoss = prefs.getString("trubLoss", "1.9");
			String equipmentLoss = prefs.getString("equipmentLoss", "3.8");
			String mashThickness = prefs.getString("mashThickness", "2.77");
			String grainTemp = prefs.getString("grainTemp", "21.1");
			String targetMashTemp = prefs.getString("targetMashTemp", "66.6");
			String wortShrinkage = prefs.getString("wortShrinkage", "4");
			String grainAbsorptionConstant = prefs.getString("grainAbsorptionConstant", "1.082");
			String percentBoilOff = prefs.getString("percentBoilOff", "10");
			
			//set the text to the values directly above
			batchSizeet.setText(batchSize);
			grainBillet.setText(grainBill);
			boilTimeet.setText(boilTime);
			trubLosset.setText(trubLoss);
			equipmentLosset.setText(equipmentLoss);
			mashThicknesset.setText(mashThickness);
			grainTempet.setText(grainTemp);
			wortShrinkageet.setText(wortShrinkage);
			targetMashTempet.setText(targetMashTemp);
			grainAbsorptionConstantet.setText(grainAbsorptionConstant);
			percentBoilOffet.setText(percentBoilOff);
			
		}else if (unit.equals("0") && !unit.equals(previousUnit)) {
			batchSizetv.setText("Batch Size (Gal) ");
			grainBilltv.setText("Grain Bill (Lbs) ");
			trubLosstv.setText("Trub Loss (Gal) ");
			equipmentLosstv.setText("Equipment Loss (Gal) ");
			mashThicknesstv.setText("Mash Thickness (Qts/lb) ");
			grainTemptv.setText("Grain Temp (F) ");
			targetMashTemptv.setText("Target Mash Temp (F) ");
			grainAbsorbtionConstanttv.setText("Grain Absorption\n Constant (Gal/lb) ");
			totalWaterNeededtv.setText("Total Water Needed (Gal): ");
			mashWaterNeededtv.setText("Mash Water Needed (Gal): ");
			spargeWaterNeededtv.setText("Sparge Water Needed (Gal): ");
			strikeWaterTemptv.setText("Strike Temp (F): ");
			preBoilVolumetv.setText("Pre-Boil Wort Volume (Gal): ");
			
			batchSizeet.setText("5");
			grainBillet.setText("10");
			boilTimeet.setText("60");
			trubLosset.setText(".5");
			equipmentLosset.setText("1");
			mashThicknesset.setText("1.33");
			grainTempet.setText("70");
			targetMashTempet.setText("152");
			wortShrinkageet.setText("4");
			grainAbsorptionConstantet.setText("0.13");
			percentBoilOffet.setText("10");
			
		} else if (unit.equals("1") && !unit.equals(previousUnit)) {
			batchSizetv.setText("Batch Size (Litre) ");
			grainBilltv.setText("Grain Bill (Kg) ");
			trubLosstv.setText("Trub Loss (Litre) ");
			equipmentLosstv.setText("Equipment Loss (Litre) ");
			mashThicknesstv.setText("Mash Thickness (Litre/Kg) ");
			grainTemptv.setText("Grain Temp (C) ");
			targetMashTemptv.setText("Target Mash Temp (C) ");
			grainAbsorbtionConstanttv.setText("Grain Absorption\n Constant (Litre/Kg) ");
			grainAbsorptionConstantet.setText("1.082");
			totalWaterNeededtv.setText("Total Water Needed (Litre): ");
			mashWaterNeededtv.setText("Mash Water Needed (Litre): ");
			spargeWaterNeededtv.setText("Sparge Water Needed (Litre): ");
			strikeWaterTemptv.setText("Strike Temp (C): ");
			preBoilVolumetv.setText("Pre-Boil Wort Volume (Litre): ");
			
			batchSizeet.setText("19");
			grainBillet.setText("4.5");
			boilTimeet.setText("60");
			trubLosset.setText("1.9");
			equipmentLosset.setText("3.8");
			mashThicknesset.setText("2.77");
			grainTempet.setText("21.1");
			targetMashTempet.setText("66.6");
			wortShrinkageet.setText("4");
			grainAbsorptionConstantet.setText("1.082");
			percentBoilOffet.setText("10");
		}
	}

}
