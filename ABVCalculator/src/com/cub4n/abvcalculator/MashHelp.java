package com.cub4n.abvcalculator;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MashHelp extends Activity {

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mash_help);
				
		int deviceVersion = android.os.Build.VERSION.SDK_INT;
		Log.d("version", Integer.toString(deviceVersion));
		if (deviceVersion > 10) {
			ActionBar bar = getActionBar();
			ColorDrawable myColor = new ColorDrawable( Color.parseColor("#825A36"));
			bar.setBackgroundDrawable(myColor);
		}	
		
		Button source = (Button)findViewById(R.id.bMashSource);
		source.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.brew365.com/mash_sparge_water_calculator.php"));
				startActivity(browserIntent);
			}
			
		});
	}

	//i dont want the user to access the menu from the options menu
	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.mash_help, menu);
//		return true;
//	}

}
