package com.cub4n.abvcalculator;


import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;
import android.widget.Toast;

public class Prefs extends PreferenceActivity {
	
	private SharedPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.prefs);

		bindPreferenceSummaryToValue(findPreference("unit"));
		bindPreferenceSummaryToValue(findPreference("defaultActivity"));
		bindPreferenceSummaryToValue(findPreference("calibrationTemp"));
		bindPreferenceSummaryToValue(findPreference("alarm_tone"));
		bindPreferenceSummaryToValue(findPreference("myVersion"));
		
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		if (prefs.getBoolean("firstTimeStart", true)) {
			Toast.makeText(getBaseContext(),"Please initialize your settings.", Toast.LENGTH_LONG).show();
		}
	}

	private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object value) {
			String stringValue = value.toString();

			if (preference instanceof ListPreference) {
				// For list preferences, look up the correct display value in
				// the preference's 'entries' list.
				ListPreference listPreference = (ListPreference) preference;
				int index = listPreference.findIndexOfValue(stringValue);
				// Set the summary to reflect the new value.
				preference
						.setSummary(index >= 0 ? listPreference.getEntries()[index]
								: null);
			} else if (preference instanceof EditTextPreference) {
				preference.setSummary(stringValue);
			} else if (preference instanceof RingtonePreference) {
				// For ringtone preferences, look up the correct display value
				// using RingtoneManager.
				if (TextUtils.isEmpty(stringValue)) {
					// Empty values correspond to 'silent' (no ringtone).
					preference.setSummary(R.string.pref_ringtone_silent);

				} else {
					Ringtone ringtone = RingtoneManager.getRingtone(
							preference.getContext(), Uri.parse(stringValue));

					if (ringtone == null) {
						// Clear the summary if there was a lookup error.
						preference.setSummary(null);
					} else {
						// Set the summary to reflect the new ringtone display
						// name.
						String name = ringtone
								.getTitle(preference.getContext());
						preference.setSummary(name);
					}
				}

			} else {
				// For all other preferences, set the summary to the value's
				// simple string representation.
				preference.setSummary(stringValue);
			}
			return true;
		}
	};

	private static void bindPreferenceSummaryToValue(Preference preference) {
		// Set the listener to watch for value changes.
		preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

		// Trigger the listener immediately with the preference's
		// current value.
		sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getString(preference.getKey(),""));
	}
	
	@Override
	public void onBackPressed() {
		Intent startActivity;
		prefs = PreferenceManager.getDefaultSharedPreferences(this);

		//check if its the first time this application has been started
		if (prefs.getBoolean("firstTimeStart", true)) {
			SharedPreferences.Editor edit = prefs.edit();
			edit.putBoolean("firstTimeStart", false);
			edit.commit();
			
			startActivity = new Intent(getBaseContext(), TabsFragmentActivity.class);
			startActivity(startActivity);
			finish(); //removes prefs from the activity stack so user cannot back into prefs
			

		} else
			super.onBackPressed(); //if its not the first time behave as if this override didnt exist
	}

}
