package com.cub4n.abvcalculator;

import java.text.DecimalFormat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Refractometer extends Fragment implements OnClickListener {
	
	final Context context = this.getActivity();

	double percentAlcohol, alcoholByWeight, OB, FB, OG, FG, wortCorrection;
	boolean flag;
	int caloriesPerBeer, attentuation;
	
	AlertDialog checkUpdateDialog;

	DecimalFormat df = new DecimalFormat("#.##");
	DecimalFormat oneDecimal = new DecimalFormat("#.#");
	DecimalFormat gravity = new DecimalFormat("#.###");

	Button calculate;
	EditText OriginalBrix, FinalBrix, WortCorrection;
	TextView ABV, ABW, initialGravity, finalGravity, calories, updateDialogText, atten;
	SharedPreferences prefs;
	String unit;
	
	//create object for to use formulas
	Formulas formula = new Formulas();
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.bCalculate:
						
			flag = false;
			// try catch for the case where they only want to convert 
			try{
				// parse values
				OB = Double.parseDouble(OriginalBrix.getText().toString());
				wortCorrection = Double.parseDouble(WortCorrection.getText().toString());
				
				//make sure FB field is empty
				try{
					FB = Double.parseDouble(FinalBrix.getText().toString());
				}
				catch(NumberFormatException e){
					flag = true;
				}
				
				//Calculate Brix to Gravity calculation
				OG = formula.OBtoOG(OB, wortCorrection);
				
				
				//update textView
				initialGravity.setText("Original Gravity: " + gravity.format(OG));
			}
			catch(NumberFormatException e){
				Toast.makeText(getActivity(), "Sorry you didn't type anything", Toast.LENGTH_LONG).show();
				reset();
			}
			
			
			
			//if flag == true then previous try catch was successful and they only wanted to calculate OB to OG
			if (flag != true){
				try {
					// parse values
					OB = Double.parseDouble(OriginalBrix.getText().toString());
					FB = Double.parseDouble(FinalBrix.getText().toString());
					wortCorrection = Double.parseDouble(WortCorrection.getText().toString());
	
					// calculate original and final brix
					OG = formula.OBtoOG(OB, wortCorrection);
					FG = formula.FBtoFG(OB, FB);
					
					caloriesPerBeer = formula.calorie(OG, FG, unit);
	
					// calculate abv and abw
					percentAlcohol = formula.calculateABV(OG, FG);
					alcoholByWeight = formula.calculateABW(percentAlcohol, FG);
					
					//calculate attenuation
					attentuation = formula.attenuation(OG, FG);
	
					// set text for apropriate fields
					ABV.setText("Calculated ABV: " + oneDecimal.format(percentAlcohol) + "%");
					ABW.setText("Calculated ABW: " + oneDecimal.format(alcoholByWeight) + "%");
					initialGravity.setText("Original Gravity: " + gravity.format(OG));
					finalGravity.setText("Final Gravity: " + gravity.format(FG));
					atten.setText("Attentuation: " + attentuation + "%");
					
					
					//unit = 0; F
					//unit = 1; C
					if(unit.equals("0"))
						calories.setText("Calories Per Beer (12oz): " + caloriesPerBeer);
					else if(unit.equals("1"))
						calories.setText("Calories Per Beer (330ml): " + caloriesPerBeer);
					
	
				} catch (NumberFormatException e) {
					Toast.makeText(getActivity(), "Sorry you didn't type anything", Toast.LENGTH_SHORT).show();
					reset();
				}
			}
			break;
		}

	}	
	
	 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	        if (container == null) {
	            // We have different layouts, and in one of them this
	            // fragment's containing frame doesn't exist.  The fragment
	            // may still be created from its saved state, but there is
	            // no reason to try to create its view hierarchy because it
	            // won't be displayed.  Note this is not needed -- we could
	            // just run the code below, where we would create and return
	            // the view hierarchy; it would just never be used.
	            return null;
	        }
	        
	        
	        View V = inflater.inflate(R.layout.activity_refractometer, container, false);
	        
			//pass through the inflater, and container to use for the findViewById()
	        initialize(V);
	        
	        //set up on touch listener to any non-editText fields to hide keyboard when its touched
	        setupUI(V);
	        
	        setHasOptionsMenu(true);
	        return V;
	    }

	public void initialize(View V) {
		
		// set up dialog and text view for version check
		checkUpdateDialog = new AlertDialog.Builder(getActivity()).create();

		View updateDialogView = getActivity().getLayoutInflater().inflate(R.layout.check_update_dialog, null);

		updateDialogText = (TextView) updateDialogView .findViewById(R.id.tvUpdateText);
		
		// grab preferences
		prefs = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
		
		// checks version for update
		formula.checkUpdate(prefs, updateDialogText, checkUpdateDialog, updateDialogView);
					
		//initialize button calculating
		calculate = (Button) V.findViewById(R.id.bCalculate);
		calculate.setOnClickListener(this);

		//initialize EditTexts and TextViews for display
		OriginalBrix = (EditText) V.findViewById(R.id.etInitialBrix);
		FinalBrix = (EditText) V.findViewById(R.id.etFinalBrix);
		WortCorrection = (EditText) V.findViewById(R.id.etWortCorrection);
		ABV = (TextView) V.findViewById(R.id.tvABV);
		ABW = (TextView) V.findViewById(R.id.tvABW);
		initialGravity = (TextView) V.findViewById(R.id.tvOriginalGravity);
		finalGravity = (TextView) V.findViewById(R.id.tvFinalGravity);
		calories = (TextView) V.findViewById(R.id.tvCaloriesPerBeer1);
		atten = (TextView) V.findViewById(R.id.tvAttentuation);
		
		// if tempurature units are in Celsius convert to Fahrenheit
		unit = prefs.getString("unit", "");
		Log.d("Unit", unit);
		// fahrenheit: unit = 0; Celcius: unit = 1	
		
		
	}
	
	public void reset(){
		ABV.setText("Calculated ABV: 0.0%");
		initialGravity.setText("Original Gravity: 0.000");
		finalGravity.setText("Final Gravity: 0.000");
		ABW.setText("Calculated ABW: 0.0%");
		if(unit.equals("0"))
			calories.setText("Calories Per Beer (12oz): 0");
		else if(unit.equals("1"))
			calories.setText("Calories Per Beer (330ml): 0");
	}
	
	// sets menu entries from optionsmenu.xml in the menu directory
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// Inflate the menu; this adds items to the action bar if it is present.

		inflater.inflate(R.menu.optionsmenu, menu);
	}
	@Override
	public void onResume(){
		super.onResume();
		
		unit = prefs.getString("unit", "");
		reset();
	}

	//assigns actions for each menu entry
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case R.id.preferences:
			intent = new Intent(this.getActivity(), Prefs.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
			
		case R.id.information:
			intent = new Intent(this.getActivity(), Info.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		case R.id.changelog:
			intent = new Intent(this.getActivity(), Changelog.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		case R.id.contact:
			Intent email = new Intent(Intent.ACTION_SEND);
			email.putExtra(Intent.EXTRA_EMAIL, new String[]{"brewbuddydev@gmail.com"});		  
			email.putExtra(Intent.EXTRA_SUBJECT, "Brew Buddy: 'add subject here'");
			email.setType("message/rfc822");
			startActivity(Intent.createChooser(email, "Choose an Email client :"));
			break;
//		case R.id.donate:
//			intent = new Intent(this.getActivity(), .class);
//			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			startActivity(intent);
//			break;
			
		}
		return super.onOptionsItemSelected(item);
	}
	
	public static void hideSoftKeyboard(Activity activity){
		//hide keyboard after button pressed
		InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	
	public void setupUI(View view) {

	    //Set up touch listener for non-text box views to hide keyboard.
	    if(!(view instanceof EditText)) {
	        view.setOnTouchListener(new OnTouchListener() {
	            public boolean onTouch(View v, MotionEvent event) {
	                hideSoftKeyboard(getActivity());
	                return false;
	            }
	        });
	    }

	    //If a layout container, iterate over children and seed recursion.
	    if (view instanceof ViewGroup) {

	        for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
	            View innerView = ((ViewGroup) view).getChildAt(i);
	            setupUI(innerView);
	        }
	    }
	}
	

}
