package com.cub4n.abvcalculator;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

public class Splash extends Activity {

	Intent startActivity;
	Formulas formula = new Formulas();
	
	// grab preferences
	SharedPreferences prefs;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		int deviceVersion = android.os.Build.VERSION.SDK_INT;
		Log.d("version", Integer.toString(deviceVersion));
		if (deviceVersion > 10) {
			ActionBar bar = getActionBar();
			bar.hide();
		}
		Thread timer = new Thread() {
			public void run() {
				try {
					sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally{
					if (prefs.getBoolean("firstTimeStart", true)) {
						//this fixes the version not showing on first time start.
						formula.updateVersion(formula.getVersion(), formula.getMyVersion(), prefs);
						
						startActivity = new Intent(getBaseContext(), Prefs.class);
						startActivity(startActivity);
					} 
					else {
						startActivity = new Intent(getBaseContext(),TabsFragmentActivity.class);
						startActivity(startActivity);
					}
				}
			}
		};
		timer.start();
		
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}

}
