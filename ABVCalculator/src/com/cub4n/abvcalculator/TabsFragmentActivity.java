package com.cub4n.abvcalculator;

import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;

public class TabsFragmentActivity extends FragmentActivity {

	SharedPreferences prefs;
	String defaultActivity;

	private ViewPager mViewPager;
	private PagerAdapter mPagerAdapter;

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tabs_layout);

		int deviceVersion = android.os.Build.VERSION.SDK_INT;
		Log.d("version", Integer.toString(deviceVersion));
		if (deviceVersion > 10) {
			ActionBar bar = getActionBar();
			ColorDrawable myColor = new ColorDrawable(Color.parseColor("#603814"));
			bar.setBackgroundDrawable(myColor);
		}

		// Intialise ViewPager
		this.intialiseViewPager();

	}

	@SuppressLint("NewApi")
	private void intialiseViewPager() {

		List<Fragment> fragments = new Vector<Fragment>();
		fragments.add(Fragment.instantiate(this, Hydrometer.class.getName()));
		fragments.add(Fragment.instantiate(this, Refractometer.class.getName()));
		fragments.add(Fragment.instantiate(this, Mash.class.getName()));
		// fragments.add(Fragment.instantiate(this, HopScheduler.class.getName()));
		
		this.mPagerAdapter = new PagerAdapter(super.getSupportFragmentManager(), fragments);
		this.mViewPager = (ViewPager) super.findViewById(R.id.pager);
		//to change the color of the scroll bar, go to 
		this.mViewPager.setAdapter(this.mPagerAdapter);

		// grab preferences for app
		prefs = PreferenceManager.getDefaultSharedPreferences(this);

		defaultActivity = prefs.getString("defaultActivity", "0");
		Log.d("defaultActivity", defaultActivity);

		// Default to first tab
		// if defaultActivity=0 start hydrometer
		if (defaultActivity.equals("0")) {
			this.mViewPager.setCurrentItem(0);
		} else {
			this.mViewPager.setCurrentItem(1);
		}
	}

}
